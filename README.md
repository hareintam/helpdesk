# Prerequisites

1. Java 8+ SDK
2. Node 10

# Running on localhost

0. Navigate to project root
1. Start Spring Boot backend: `cd backend && ./gradlew bootRun` (on Windows use `gradlew bootRun`)
2. Open a new terminal window in project root
3. Start React frontend: `cd frontend && npm install && npm start`
4. Browser should have opened with test assignment solution (http://localhost:8080)

This test assignment uses an in memory database. Data will be lost when Spring Boot backend is shut down.