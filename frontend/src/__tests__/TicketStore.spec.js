import axios from 'axios';
import { when } from 'jest-when';
import TicketStore from '../TicketStore';

jest.mock('axios');
const TICKETS = [{
  id: 24545,
  title: 'Big Problem',
  priority: '5',
  status: 'NEW',
  created: '02.10.18 10:50',
  description: 'There is a big problem.',
  email: 'email@email.com',
},
{
  id: 24546,
  title: 'Big Problem',
  priority: '5',
  status: 'IN PROGRESS',
  created: '02.10.18 10:50',
  description: 'There is a big problem.',
  email: 'email@email.com',
}];

const NEW_TICKET = {
  title: 'Big Problem',
  priority: '5',
  description: 'There is a big problem.',
  email: 'email@email.com',
};

const UPDATED_TICKET = {
  id: 24545,
  title: 'Big Problem',
  priority: '3',
  status: 'IN PROGRESS',
  created: '02.10.18 10:50',
  edited: '02.11.18 10:50',
  description: 'There is a big problem.',
  email: 'email@email.com',
};

const API_TICKET = {
  id: 24545,
  title: 'Big Problem',
  priority: 3,
  status: 'IN_PROGRESS',
  created: '02.10.18 10:50',
  edited: '02.11.18 10:50',
  description: 'There is a big problem.',
  email: 'email@email.com',
};

describe('TicketStore', () => {
  describe('constructor', () => {
    test('calls init() method', () => {
      jest.spyOn(TicketStore.prototype, 'init').mockImplementation();
      const ticketStore = new TicketStore();

      expect(ticketStore.init).toHaveBeenCalled();
    });
  });

  describe('init', () => {
    test('gets all tickets', async () => {
      const ticketStore = new TicketStore();
      ticketStore.getAllTickets = jest.fn();

      await ticketStore.init();

      expect(ticketStore.getAllTickets).toHaveBeenCalled();
    });
  });

  describe('getAllTickets', () => {
    test('fetches all tickets and assigns them to ticketStore.tickets', async () => {
      const ticketStore = new TicketStore();
      const response = { data: TICKETS };
      axios.get.mockResolvedValue(response);

      await ticketStore.getAllTickets();

      expect(axios.get).toHaveBeenCalled();
      expect(ticketStore.tickets).toEqual(TICKETS);
    });
  });

  describe('updateTicket', () => {
    test('updates ticket and returns updated ticket', async () => {
      const ticketStore = new TicketStore();
      const response = { data: TICKETS[0] };
      axios.put = jest.fn();
      when(axios.put).calledWith('http://localhost:8081/tickets', TICKETS[0]).mockReturnValue(response);

      const result = await ticketStore.updateTicket(TICKETS[0]);

      expect(axios.put).toHaveBeenCalled();
      expect(result).toEqual(TICKETS[0]);
    });
  });

  describe('createTicket', () => {
    test('creates ticket and returns created ticket', async () => {
      const ticketStore = new TicketStore();
      const response = { data: API_TICKET };
      axios.post = jest.fn();
      when(axios.post).calledWith('http://localhost:8081/tickets', API_TICKET).mockReturnValue(response);

      const result = await ticketStore.createTicket(UPDATED_TICKET);

      expect(axios.post).toHaveBeenCalled();
      expect(result).toEqual(API_TICKET);
    });
  });

  describe('formatTicketsForUi', () => {
    test('return formatted tickets for UI', () => {
      const ticketStore = new TicketStore();
      const apiTicket = {
        id: 24545,
        title: 'Big Problem',
        priority: 3,
        status: 'IN_PROGRESS',
        created: '02.10.18 10:50',
        edited: '02.11.18 10:50',
        description: 'There is a big problem.',
        email: 'email@email.com',
      };
      const uiTicket = {
        id: 24545,
        title: 'Big Problem',
        priority: '3',
        status: 'IN PROGRESS',
        created: '02.10.18 10:50',
        edited: '02.11.18 10:50',
        description: 'There is a big problem.',
        email: 'email@email.com',
      };

      const result = ticketStore.formatTicketsForUi([apiTicket]);

      expect(result).toEqual([uiTicket]);
    });
  });

  describe('formatTicketForApi', () => {
    test('format status and priority fields for API', () => {
      const ticketStore = new TicketStore();
      const apiTicket = {
        id: 24545,
        title: 'Big Problem',
        priority: 3,
        status: 'IN_PROGRESS',
        created: '02.10.18 10:50',
        edited: '02.11.18 10:50',
        description: 'There is a big problem.',
        email: 'email@email.com',
      };
      const uiTicket = {
        id: 24545,
        title: 'Big Problem',
        priority: '3',
        status: 'IN PROGRESS',
        created: '02.10.18 10:50',
        edited: '02.11.18 10:50',
        description: 'There is a big problem.',
        email: 'email@email.com',
      };

      const result = ticketStore.formatTicketForApi(uiTicket);

      expect(result).toEqual(apiTicket);
    });
  });

  describe('getActiveTicket', () => {
    test('returns active ticket if activeTicketId is present', () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = TICKETS;
      ticketStore.activeTicketId = 24545;

      const result = ticketStore.getActiveTicket();

      expect(result).toEqual(TICKETS[0]);
    });
    test('returns empty ticket if activeTicketId is null', () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = TICKETS;
      ticketStore.activeTicketId = null;
      const expected = {
        title: '', email: '', description: '', priority: '', status: '', created: '', edited: '',
      };

      const result = ticketStore.getActiveTicket();

      expect(result).toEqual(expected);
    });
    test('returns empty ticket if activeTicketId is empty', () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = TICKETS;
      ticketStore.activeTicketId = '';
      const expected = {
        title: '', email: '', description: '', priority: '', status: '', created: '', edited: '',
      };

      const result = ticketStore.getActiveTicket();

      expect(result).toEqual(expected);
    });
  });

  describe('openModal', () => {
    test('sets isModalVisible and activeTicketId', () => {
      const ticketStore = new TicketStore();

      ticketStore.openModal(24545);

      expect(ticketStore.isModalVisible).toEqual(true);
      expect(ticketStore.activeTicketId).toEqual(24545);
    });
  });

  describe('closeModal', () => {
    test('sets isModalVisible to false', () => {
      const ticketStore = new TicketStore();

      ticketStore.closeModal();

      expect(ticketStore.isModalVisible).toEqual(false);
    });
  });

  describe('saveTicket', () => {
    test('updates ticket in backend and in ticketStore.tickets if ticket id is present', async () => {
      const ticketStore = new TicketStore();
      ticketStore.activeTicketId = 24545;
      ticketStore.setTicketFields = jest.fn();
      ticketStore.updateTicket = jest.fn();
      when(ticketStore.updateTicket).calledWith(TICKETS[0]).mockReturnValue(TICKETS[0]);
      ticketStore.replaceInTickets = jest.fn();

      await ticketStore.saveTicket(TICKETS[0], true);

      expect(ticketStore.setTicketFields).toHaveBeenCalled();
      expect(ticketStore.updateTicket).toHaveBeenCalled();
      expect(ticketStore.replaceInTickets).toHaveBeenCalledWith(TICKETS[0]);
      expect(ticketStore.activeTicketId).toEqual(null);
    });
    test('created ticket in backend and adds ticket to ticketStore.tickets if ticket id is not present', async () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = [];
      ticketStore.activeTicketId = '';
      ticketStore.setTicketFields = jest.fn();
      ticketStore.createTicket = jest.fn();
      when(ticketStore.createTicket).calledWith(NEW_TICKET).mockReturnValue(TICKETS[0]);

      await ticketStore.saveTicket(NEW_TICKET, true);

      expect(ticketStore.setTicketFields).toHaveBeenCalled();
      expect(ticketStore.createTicket).toHaveBeenCalled();
      expect(ticketStore.tickets).toEqual([TICKETS[0]]);
      expect(ticketStore.activeTicketId).toEqual(null);
    });
  });

  describe('replaceInTickets', () => {
    test('replaces updated ticket in ticketStore.tickets', () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = TICKETS;
      ticketStore.getTicketIndex = jest.fn(() => 0);

      ticketStore.replaceInTickets(UPDATED_TICKET);

      expect(ticketStore.getTicketIndex).toHaveBeenCalled();
      expect(ticketStore.tickets[0]).toEqual(UPDATED_TICKET);
    });
  });

  describe('getTicketIndex', () => {
    test('returns active ticket index', () => {
      const ticketStore = new TicketStore();
      ticketStore.tickets = { indexOf: jest.fn() };
      when(ticketStore.tickets.indexOf).calledWith(TICKETS[0]).mockReturnValue(0);
      ticketStore.getActiveTicket = jest.fn(() => TICKETS[0]);

      expect(ticketStore.getTicketIndex()).toEqual(0);
      expect(ticketStore.getActiveTicket).toHaveBeenCalled();
    });
  });

  describe('setTicketFields', () => {
    test('sets missing ticket fields to ticket form values', () => {
      const ticketStore = new TicketStore();
      ticketStore.getActiveTicket = jest.fn(() => TICKETS[0]);

      ticketStore.setTicketFields(NEW_TICKET);

      expect(TICKETS[0].status).toEqual('NEW');
      expect(TICKETS[0].id).toEqual(24545);
      expect(TICKETS[0].created).toEqual('02.10.18 10:50');
    });
  });

  describe('closeTicket', () => {
    test('updates ticket in backend and removes it from ticketStore.tickets', async () => {
      const ticketStore = new TicketStore();
      const ticket = {
        id: 24545,
        title: 'Big Problem',
        priority: '5',
        status: 'NEW',
        created: '02.10.18 10:50',
        description: 'There is a big problem.',
        email: 'email@email.com',
      };
      ticketStore.tickets = { filter: jest.fn(() => []) };
      ticketStore.updateTicket = jest.fn();

      await ticketStore.closeTicket(ticket);

      expect(ticketStore.updateTicket).toHaveBeenCalledWith(ticket);
      expect(ticketStore.tickets).toEqual([]);
    });
  });

  describe('sortTicketsByPriority', () => {
    test('assigns tickets sorted by priority to ticketStore.tickets and reverses sort order', () => {
      const ticketStore = new TicketStore();
      ticketStore.getTicketsSortedBy = jest.fn();
      when(ticketStore.getTicketsSortedBy).calledWith('priority', true).mockReturnValue(TICKETS);

      ticketStore.sortTicketsByPriority();

      expect(ticketStore.getTicketsSortedBy).toHaveBeenCalledWith('priority', true);
      expect(ticketStore.isPriorityAsc).toEqual(false);
      expect(ticketStore.tickets).toEqual(TICKETS);
    });
  });

  describe('sortTicketsByStatus', () => {
    test('assigns tickets sorted by status to ticketStore.tickets and reverses sort order', () => {
      const ticketStore = new TicketStore();
      ticketStore.getTicketsSortedBy = jest.fn();
      when(ticketStore.getTicketsSortedBy).calledWith('status', true).mockReturnValue(TICKETS);

      ticketStore.sortTicketsByStatus();

      expect(ticketStore.getTicketsSortedBy).toHaveBeenCalledWith('status', true);
      expect(ticketStore.isStatusAsc).toEqual(false);
      expect(ticketStore.tickets).toEqual(TICKETS);
    });
  });

  describe('sortTicketsByCreatedDate', () => {
    test('assigns tickets sorted by created date to ticketStore.tickets and reverses sort order', () => {
      const ticketStore = new TicketStore();
      ticketStore.getTicketsSortedBy = jest.fn();
      when(ticketStore.getTicketsSortedBy).calledWith('created', true).mockReturnValue(TICKETS);

      ticketStore.sortTicketsByCreatedDate();

      expect(ticketStore.getTicketsSortedBy).toHaveBeenCalledWith('created', true);
      expect(ticketStore.isCreatedDateAsc).toEqual(false);
      expect(ticketStore.tickets).toEqual(TICKETS);
    });
  });

  describe('sortTicketsByEditedDate', () => {
    test('assigns tickets sorted by edited date to ticketStore.tickets and reverses sort order', () => {
      const ticketStore = new TicketStore();
      ticketStore.getTicketsSortedBy = jest.fn();
      when(ticketStore.getTicketsSortedBy).calledWith('edited', true).mockReturnValue(TICKETS);

      ticketStore.sortTicketsByEditedDate();

      expect(ticketStore.getTicketsSortedBy).toHaveBeenCalledWith('edited', true);
      expect(ticketStore.isEditedDateAsc).toEqual(false);
      expect(ticketStore.tickets).toEqual(TICKETS);
    });
  });

  describe('getTicketsSortedBy', () => {
    test('sorts tickets by given field and in ascending order if isAscending is true', () => {
      const ticketStore = new TicketStore();
      const tickets = TICKETS.slice();
      const statusNew = tickets[0];
      const statusInProgress = tickets[1];
      ticketStore.tickets = tickets;

      const result = ticketStore.getTicketsSortedBy('status', true);

      expect(result[0]).toEqual(statusInProgress);
      expect(result[1]).toEqual(statusNew);
    });
    test('sorts tickets by given field and in descending order if isAscending is false', () => {
      const ticketStore = new TicketStore();
      const tickets = TICKETS.slice();
      const statusNew = tickets[0];
      const statusInProgress = tickets[1];
      ticketStore.tickets = tickets;

      const result = ticketStore.getTicketsSortedBy('status', false);

      expect(result[0]).toEqual(statusNew);
      expect(result[1]).toEqual(statusInProgress);
    });
  });
});
