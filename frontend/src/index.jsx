import React from 'react';
import './styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {render} from 'react-dom';
import {Provider} from 'mobx-react';
import TicketStore from './TicketStore';
import HelpDeskApp from './components/HelpDeskApp';

render(
  <div>
    <Provider ticketStore={new TicketStore()}>
      <HelpDeskApp/>
    </Provider>
  </div>,
  document.getElementById('root'),
);
