import React from 'react';
import renderer from 'react-test-renderer';
import {Provider} from 'mobx-react';
import HelpDeskApp from '../HelpDeskApp';
import TicketStore from '../../TicketStore';

it('HelpDeskApp renders correctly', () => {
  const helpDeskApp = renderer
    .create(<Provider ticketStore={new TicketStore()}><HelpDeskApp/></Provider>)
    .toJSON();
  expect(helpDeskApp).toMatchSnapshot();
});
