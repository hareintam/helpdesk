import React from 'react';
import renderer from 'react-test-renderer';
import TicketStore from '../../TicketStore';
import TicketList from '../TicketList';

it('TicketList is not rendered if no tickets present', () => {
  const ticketsList = renderer
    .create(<TicketList ticketStore={new TicketStore()}/>)
    .toJSON();
  expect(ticketsList).toMatchSnapshot();
});

it('TicketList renders if there is at least one ticket present', () => {
  const ticketStore = new TicketStore();
  ticketStore.tickets = [{
    id: 24545,
    title: 'Big Problem',
    priority: '5',
    status: 'NEW',
    created: '02.10.18 10:50',
    description: 'There is a big problem.',
    email: 'email@email.com',
  }];
  const ticketsList = renderer
    .create(<TicketList ticketStore={ticketStore}/>)
    .toJSON();
  expect(ticketsList).toMatchSnapshot();
});
