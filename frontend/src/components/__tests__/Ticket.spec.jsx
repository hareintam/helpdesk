import React from 'react';
import renderer from 'react-test-renderer';
import TicketStore from '../../TicketStore';
import Ticket from '../Ticket';

it('Ticket modal is rendered if isModalVisible is true', () => {
  const ticketStore = new TicketStore();
  ticketStore.tickets = [{
    id: 24545,
    title: 'Big Problem',
    priority: '5',
    status: 'NEW',
    created: '02.10.18 10:50',
    description: 'There is a big problem.',
    email: 'email@email.com',
  }];
  ticketStore.isModalVisible = true;
  ticketStore.activeTicketId = 24545;
  const ticketModal = renderer
    .create(<Ticket ticketStore={ticketStore}/>)
    .toJSON();
  expect(ticketModal).toMatchSnapshot();
});

it('Empty ticket modal is rendered if isModalVisible is true and activeTicketId is empty', () => {
  const ticketStore = new TicketStore();
  ticketStore.activeTicketId = '';
  ticketStore.isModalVisible = true;
  const ticketModal = renderer
    .create(<Ticket ticketStore={ticketStore}/>)
    .toJSON();
  expect(ticketModal).toMatchSnapshot();
});

it('Ticket modal is not rendered if isModalVisible is false', () => {
  const ticketStore = new TicketStore();
  ticketStore.isModalVisible = false;
  const ticketModal = renderer
    .create(<Ticket ticketStore={ticketStore}/>)
    .toJSON();
  expect(ticketModal).toMatchSnapshot();
});
