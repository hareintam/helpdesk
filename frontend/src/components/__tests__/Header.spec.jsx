import React from 'react';
import renderer from 'react-test-renderer';
import Header from '../Header';
import TicketStore from '../../TicketStore';

it('Header renders correctly', () => {
  const header = renderer
    .create(<Header ticketStore={new TicketStore()}/>)
    .toJSON();
  expect(header).toMatchSnapshot();
});
