import React from 'react'
import {Formik} from 'formik';
import {Form, Input, Radio, Textarea, Toggle} from 'react-formik-ui';
import {inject, observer} from "mobx-react";
import * as yup from 'yup';

@inject("ticketStore")
@observer
class Ticket extends React.Component {

  onSubmit = (data) => {
    const {ticketStore} = this.props;
    ticketStore.saveTicket(data);
    ticketStore.closeModal();
  };

  getSchema = () => {
    return yup.object().shape({
      title: yup.string()
        .required('Enter title'),
      email: yup.string()
        .email('Enter valid email')
        .required('Enter email'),
      description: yup.string()
        .required('Enter description'),
      priority: yup.string()
        .required('Choose priority level'),
    })
  };

  getTicketHeader = () => {
    const {ticketStore} = this.props;
    const {created, edited} = ticketStore.getActiveTicket();
    return (
      <div className="ticketHeader">
        <h3 className="ticketNumber">Ticket #{ticketStore.activeTicketId}</h3>
        <div className="ticketDates">
          {created ? <label>Created: {created}</label> : null}
          {edited ? <br/> : null}
          {edited ? <label>Edited: {edited}</label> : null}
        </div>
      </div>);
  };

  getTicketButtons = () => {
    const {ticketStore} = this.props;
    return (
      <div className="ticketButtons">
        <button className="btn btn-success ticketButton" type="submit">Save</button>
        <button className="btn btn-warning ticketButton"
                onClick={() => ticketStore.closeModal()}>Cancel
        </button>
      </div>
    );
  };

  render() {
    const {ticketStore} = this.props;
    if (!ticketStore.isModalVisible) {
      return null;
    }
    const {created, edited, title, email, description, priority, status} = ticketStore.getActiveTicket();
    const isInProgress = status === "IN PROGRESS";
    return (
      <div className="ticketModal">
        <Formik
          initialValues={{created, edited, title, email, description, priority, isInProgress}}
          onSubmit={this.onSubmit}
          enableReinitialize={true}
          validationSchema={this.getSchema}
          validateOnChange={false}
        >
          {(props) => (
            <Form mode='themed'>
              {this.getTicketHeader()}
              <Input name='title' label='Title'/>
              <Input name='email' label='Email'/>
              <Textarea name='description' label='Description'/>
              <Radio inline name='priority' label='Priority'
                     options={[
                       {value: '1', label: '1'},
                       {value: '2', label: '2'},
                       {value: '3', label: '3'},
                       {value: '4', label: '4'},
                       {value: '5', label: '5'}]}
              />
              <label>Is in progress?</label>
              <Toggle name='isInProgress' className="inProgressToggle"/>
              {this.getTicketButtons()}
            </Form>)}
        </Formik>
      </div>)
  }
}

export default Ticket;