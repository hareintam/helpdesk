import React from 'react'
import {inject, observer} from "mobx-react";
import {FaSort} from "react-icons/fa";

@inject("ticketStore")
@observer
class TicketList extends React.Component {

  getTicketsTable = () => {
    const {ticketStore} = this.props;
    const tickets = this.getTableBody();
    return (
      <div>
        <table className="table ticketTable">
          <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th className="sortable"><a onClick={() => ticketStore.sortTicketsByPriority()}>Priority<FaSort/></a></th>
            <th className="sortable"><a onClick={() => ticketStore.sortTicketsByStatus()}>Status<FaSort/></a></th>
            <th className="sortable"><a onClick={() => ticketStore.sortTicketsByCreatedDate()}>Created<FaSort/></a></th>
            <th className="sortable"><a onClick={() => ticketStore.sortTicketsByEditedDate()}>Edited<FaSort/></a></th>
            <th>Edit</th>
            <th>Close</th>
          </tr>
          </thead>
          <tbody>
          {tickets}
          </tbody>
        </table>
      </div>
    );

  };

  getTableBody = () => {
    const {ticketStore} = this.props;
    return ticketStore.tickets.map(ticket =>
      <tr key={ticket.id}>
        <td>{ticket.id}</td>
        <td>{ticket.title}</td>
        <td>{ticket.priority}</td>
        <td>{ticket.status}</td>
        <td>{ticket.created}</td>
        <td>{ticket.edited}</td>
        <td>
          <button className="btn btn-success" onClick={() => ticketStore.openModal(ticket.id)}>Edit</button>
        </td>
        <td>
          <button className="btn btn-warning" onClick={() => ticketStore.closeTicket(ticket)}>Close</button>
        </td>
      </tr>
    )
  };

  render() {
    const {ticketStore} = this.props;
    const ticketsTable = ticketStore.tickets.length > 0 ? this.getTicketsTable() : null;
    return (
      <React.Fragment>
        {ticketsTable}
      </React.Fragment>
    )
  }
}

export default TicketList;