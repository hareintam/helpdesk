import React, {Component} from 'react'
import TicketList from "./TicketList";
import Ticket from "./Ticket";
import Header from "./Header";

class HelpDeskApp extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <Ticket/>
        <TicketList/>
      </div>
    )
  }
}

export default HelpDeskApp;