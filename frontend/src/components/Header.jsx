import React, {Component} from 'react'
import {FaComments, FaHeadphonesAlt, FaMailBulk} from 'react-icons/fa';
import {inject, observer} from "mobx-react";

@inject("ticketStore")
@observer
class Header extends Component {
  render() {
    const {ticketStore} = this.props;
    return (
      <div>
        <div className="appHeader">
          <h1>SERVICE DESK</h1>
          <h1><FaComments/> <FaMailBulk/> <FaHeadphonesAlt/></h1>
        </div>
        <div className="appHeader">
          <button className="btn btn-success" onClick={() => ticketStore.openModal("")}>Add New Ticket</button>
        </div>
      </div>
    )
  }
}

export default Header;