import axios from 'axios';
import { observable } from 'mobx';
import * as _ from 'underscore';

const API_URL = 'http://localhost:8081';

class TicketStore {
  @observable
  tickets = [];

  @observable
  isModalVisible = false;

  @observable
  activeTicketId = null;

  isPriorityAsc = true;

  isStatusAsc = true;

  isCreatedDateAsc = true;

  isEditedDateAsc = true;

  constructor() {
    this.init();
  }

  async init() {
    await this.getAllTickets();
  }

  async getAllTickets() {
    try {
      const response = await axios.get(`${API_URL}/tickets`);
      this.tickets = this.formatTicketsForUi(response.data);
    } catch (e) {
      console.error('Getting all tickets failed', e);
    }
  }

  async updateTicket(ticket) {
    try {
      const response = await axios.put(`${API_URL}/tickets`, this.formatTicketForApi(ticket));
      return this.formatTicketForUi(response.data);
    } catch (e) {
      console.error('Updating ticket failed', e);
      throw e;
    }
  }

  async createTicket(ticket) {
    try {
      const response = await axios.post(`${API_URL}/tickets`, this.formatTicketForApi(ticket));
      return this.formatTicketForUi(response.data);
    } catch (e) {
      console.error('Creating ticket failed', e);
      throw e;
    }
  }

  formatTicketsForUi(tickets) {
    tickets.forEach((ticket) => this.formatTicketForUi(ticket));
    return tickets;
  }

  formatTicketForUi(ticket) {
    ticket.status = ticket.status.replace('_', ' ');
    ticket.priority = ticket.priority.toString();
    return ticket;
  }

  formatTicketForApi(ticket) {
    ticket.status = ticket.status.replace(' ', '_');
    ticket.priority = parseInt(ticket.priority, 10);
    return ticket;
  }

  getActiveTicket() {
    return this.activeTicketId
      ? this.tickets.filter((ticket) => ticket.id === this.activeTicketId)[0]
      : {
        title: '', email: '', description: '', priority: '', status: '', created: '', edited: '',
      };
  }

  openModal(activeTicketId) {
    if (!this.isModalVisible) {
      this.isModalVisible = true;
      this.activeTicketId = activeTicketId;
    }
  }

  closeModal() {
    this.isModalVisible = false;
  }

  async saveTicket(ticket) {
    this.setTicketFields(ticket);
    if (this.activeTicketId) {
      this.replaceInTickets(await this.updateTicket(ticket));
    } else {
      this.tickets.push(await this.createTicket(ticket));
    }
    this.activeTicketId = null;
  }

  setTicketFields(ticket) {
    ticket.id = this.activeTicketId;
    ticket.status = ticket.isInProgress ? 'IN_PROGRESS' : 'NEW';
    delete ticket.isInProgress;
    const activeTicket = this.getActiveTicket();
    ticket.created = activeTicket.created;
    ticket.edited = activeTicket.edited;
  }

  replaceInTickets(ticket) {
    this.tickets[this.getTicketIndex()] = ticket;
  }

  getTicketIndex() {
    return this.tickets.indexOf(this.getActiveTicket());
  }

  async closeTicket(ticket) {
    if (!this.isModalVisible) {
      ticket.status = 'CLOSED';
      await this.updateTicket(ticket);
      this.tickets = this.tickets.filter((arrayTicket) => arrayTicket !== ticket);
    }
  }

  sortTicketsByPriority() {
    this.tickets = this.getTicketsSortedBy('priority', this.isPriorityAsc);
    this.isPriorityAsc = !this.isPriorityAsc;
  }

  sortTicketsByStatus() {
    this.tickets = this.getTicketsSortedBy('status', this.isStatusAsc);
    this.isStatusAsc = !this.isStatusAsc;
  }

  sortTicketsByCreatedDate() {
    this.tickets = this.getTicketsSortedBy('created', this.isCreatedDateAsc);
    this.isCreatedDateAsc = !this.isCreatedDateAsc;
  }

  sortTicketsByEditedDate() {
    this.tickets = this.getTicketsSortedBy('edited', this.isEditedDateAsc);
    this.isEditedDateAsc = !this.isEditedDateAsc;
  }

  getTicketsSortedBy(field, isAscending) {
    const sorted = _.sortBy(this.tickets, field);
    return isAscending ? sorted : sorted.reverse();
  }
}

export default TicketStore;
