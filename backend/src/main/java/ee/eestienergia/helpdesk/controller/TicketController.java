package ee.eestienergia.helpdesk.controller;

import ee.eestienergia.helpdesk.domain.Message;
import ee.eestienergia.helpdesk.domain.Ticket;
import ee.eestienergia.helpdesk.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
@RequestMapping("/tickets")
@CrossOrigin(origins = "http://localhost:8080")
public class TicketController {

  private final TicketService ticketService;

  public TicketController(TicketService ticketService) {
    this.ticketService = ticketService;
  }

  @GetMapping
  public List<Ticket> getAllTickets() {
    return ticketService.getAllNewOrInProgress();
  }

  @PostMapping
  public Ticket createTicket(@Valid @RequestBody Ticket ticket) {
    return ticketService.create(ticket);
  }

  @PutMapping
  public Ticket updateTicket(@Valid @RequestBody Ticket ticket) {
    return ticketService.update(ticket);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<List> handleValidationException(MethodArgumentNotValidException ex) {
    List<Message> messages = ex.getBindingResult().getAllErrors().stream()
      .map(e -> new Message(e.getDefaultMessage())).collect(toList());
    return ResponseEntity.status(BAD_REQUEST).contentType(APPLICATION_JSON).body(messages);
  }
}
