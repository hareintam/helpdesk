package ee.eestienergia.helpdesk.repository;

import ee.eestienergia.helpdesk.domain.Status;
import ee.eestienergia.helpdesk.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
  List<Ticket> findAllByStatusNot(Status status);
}
