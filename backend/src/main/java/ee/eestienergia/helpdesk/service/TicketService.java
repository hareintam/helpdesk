package ee.eestienergia.helpdesk.service;

import ee.eestienergia.helpdesk.domain.Status;
import ee.eestienergia.helpdesk.domain.Ticket;
import ee.eestienergia.helpdesk.repository.TicketRepository;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;

@Service
public class TicketService {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = ofPattern("dd.MM.yyyy HH:mm:ss");

  private final TicketRepository ticketRepository;

  public TicketService(TicketRepository ticketRepository) {
    this.ticketRepository = ticketRepository;
  }

  public List<Ticket> getAllNewOrInProgress() {
    return ticketRepository.findAllByStatusNot(Status.CLOSED);
  }

  public Ticket create(Ticket ticket) {
    ticket.setCreated(getNow());
    return ticketRepository.save(ticket);
  }

  public Ticket update(Ticket ticket) {
    ticket.setEdited(getNow());
    return ticketRepository.save(ticket);
  }

  String getNow() {
    return now().format(DATE_TIME_FORMATTER);
  }
}
