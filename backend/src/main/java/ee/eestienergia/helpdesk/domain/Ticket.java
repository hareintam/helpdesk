package ee.eestienergia.helpdesk.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
  @Id
  @GeneratedValue
  private Long id;
  private String created;
  private String edited;
  @NotBlank(message = "title must not be blank")
  private String title;
  @NotBlank(message = "description must not be blank")
  private String description;
  @NotBlank(message = "email must not be blank")
  @Email(message = "email must be a well-formed email address")
  private String email;
  private Status status;
  @Min(value = 1, message = "priority must be between 1 and 5")
  @Max(value = 5, message = "priority must be between 1 and 5")
  private int priority;
}
