package ee.eestienergia.helpdesk.domain;

public enum Status {
  NEW, IN_PROGRESS, CLOSED
}
