package ee.eestienergia.helpdesk.domain;

import lombok.Data;
import lombok.NonNull;

@Data
public class Message {
  @NonNull
  private String message;
}
