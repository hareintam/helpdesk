package ee.eestienergia.helpdesk.service;

import ee.eestienergia.helpdesk.domain.Ticket;
import ee.eestienergia.helpdesk.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.format.DateTimeFormatter;

import static ee.eestienergia.helpdesk.domain.Status.CLOSED;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceTest {

  private static final String DATE_TIME = "13.10.2019 17.44.43";
  private static final DateTimeFormatter DATE_TIME_FORMATTER = ofPattern("dd.MM.yyyy HH:mm:ss");

  @Mock
  private Ticket ticket;
  @Mock
  private TicketRepository ticketRepository;
  @Spy
  @InjectMocks
  private TicketService ticketService;

  @Test
  public void shouldReturnAllTicketsWithoutClosedStatus() {
    when(ticketRepository.findAllByStatusNot(CLOSED)).thenReturn(singletonList(ticket));
    assertThat(ticketService.getAllNewOrInProgress()).isEqualTo(singletonList(ticket));
  }

  @Test
  public void shouldReturnCreatedTicketWithCreatedTime() {
    doReturn(DATE_TIME).when(ticketService).getNow();
    when(ticketRepository.save(ticket)).thenReturn(ticket);

    assertThat(ticketService.create(ticket)).isEqualTo(ticket);
    verify(ticket).setCreated(DATE_TIME);
  }

  @Test
  public void shouldReturnEditedTicketWithEditedTime() {
    doReturn(DATE_TIME).when(ticketService).getNow();
    when(ticketRepository.save(ticket)).thenReturn(ticket);

    assertThat(ticketService.update(ticket)).isEqualTo(ticket);
    verify(ticket).setEdited(DATE_TIME);
  }

  @Test
  public void shouldReturnFormattedDateTimeString() {
    String nowMinusOneSecond = now().minusSeconds(1).format(DATE_TIME_FORMATTER);
    String nowPlusOneSecond = now().plusSeconds(1).format(DATE_TIME_FORMATTER);
    assertThat(ticketService.getNow()).isBetween(nowMinusOneSecond, nowPlusOneSecond);
  }

}