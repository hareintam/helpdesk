package ee.eestienergia.helpdesk.repository;

import ee.eestienergia.helpdesk.domain.Ticket;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static ee.eestienergia.helpdesk.domain.Status.*;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TicketRepositoryTest {

  private static final Ticket IN_PROGRESS_TICKET = new Ticket(null, "13.10.2019 17.44.43", "13.10.2019 18.44.45", "Title", "Description", "email@email.com", IN_PROGRESS, 4);
  private static final Ticket NEW_TICKET = new Ticket(null, "13.10.2019 17.44.43", null, "Title", "Description", "email@email.com", NEW, 4);
  private static final Ticket CLOSED_TICKET = new Ticket(null, "13.10.2019 17.44.43", "13.10.2019 18.44.45", "Title", "Description", "email@email.com", CLOSED, 4);

  @Autowired
  private TicketRepository ticketRepository;

  @Test
  public void shouldReturnAllInProgressOrNewTickets() {
    ticketRepository.saveAll(asList(IN_PROGRESS_TICKET, NEW_TICKET, CLOSED_TICKET));
    List<Ticket> tickets = ticketRepository.findAllByStatusNot(CLOSED);
    assertThat(tickets.size()).isEqualTo(2);
    assertThat(tickets.get(0).getStatus()).isNotEqualTo(CLOSED);
    assertThat(tickets.get(1).getStatus()).isNotEqualTo(CLOSED);
  }

  @Test
  public void shouldUpdateTicket() {
    Ticket ticket = ticketRepository.save(NEW_TICKET);
    ticket.setStatus(IN_PROGRESS);
    ticketRepository.save(ticket);
    Ticket updated = ticketRepository.getOne(ticket.getId());
    assertThat(updated.getStatus()).isEqualTo(IN_PROGRESS);
  }
}