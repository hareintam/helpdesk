package ee.eestienergia.helpdesk.controller;

import ee.eestienergia.helpdesk.domain.Ticket;
import ee.eestienergia.helpdesk.service.TicketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketControllerTest {

  @Mock
  private Ticket ticket;
  @Mock
  private TicketService ticketService;
  @InjectMocks
  private TicketController ticketController;

  @Test
  public void shouldReturnAllTickets() {
    when(ticketService.getAllNewOrInProgress()).thenReturn(singletonList(ticket));
    assertThat(ticketController.getAllTickets()).isEqualTo(singletonList(ticket));
  }

  @Test
  public void shouldReturnCreatedTicket() {
    when(ticketService.create(ticket)).thenReturn(ticket);
    assertThat(ticketController.createTicket(ticket)).isEqualTo(ticket);
  }

  @Test
  public void shouldReturnUpdatedTicket() {
    when(ticketService.update(ticket)).thenReturn(ticket);
    assertThat(ticketController.updateTicket(ticket)).isEqualTo(ticket);
  }
}
