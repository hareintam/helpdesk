package ee.eestienergia.helpdesk.controller;

import ee.eestienergia.helpdesk.domain.Ticket;
import ee.eestienergia.helpdesk.repository.TicketRepository;
import ee.eestienergia.helpdesk.service.TicketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static ee.eestienergia.helpdesk.domain.Status.NEW;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(TicketController.class)
public class TicketControllerIntegrationTest {

  private static final Ticket TICKET = new Ticket(1L, "13.10.2019 17.44.43", null, "Title", "Description", "email@email.com", NEW, 4);
  private static final String TICKET_JSON = "{\"id\":1,\"created\":\"13.10.2019 17.44.43\",\"edited\":null,\"title\":\"Title\",\"description\":\"Description\",\"email\":\"email@email.com\",\"status\":\"NEW\",\"priority\":4}";
  private static final String TICKETS_JSON = "[" + TICKET_JSON + "]";
  private static final String ERRORS_JSON = "[{\"message\":\"description must not be blank\"},{\"message\":\"priority must be between 1 and 5\"},{\"message\":\"title must not be blank\"},{\"message\":\"email must not be blank\"}]";
  @Autowired
  private MockMvc mvc;

  @MockBean
  private TicketService ticketService;
  @MockBean
  private TicketRepository ticketRepository;

  @Test
  public void shouldReturnAllTickets() throws Exception {
    when(ticketService.getAllNewOrInProgress()).thenReturn(singletonList(TICKET));

    mvc.perform(get("/tickets")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().json(TICKETS_JSON));
  }

  @Test
  public void shouldReturnBadRequestAndErrorMessagesIfTicketIsNotValid() throws Exception {
    mvc.perform(post("/tickets")
      .content("{}")
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isBadRequest())
      .andExpect(content().json(ERRORS_JSON));
  }

  @Test
  public void shouldReturnCreatedTicket() throws Exception {
    when(ticketService.create(TICKET)).thenReturn(TICKET);

    mvc.perform(post("/tickets")
      .content(TICKET_JSON)
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().json(TICKET_JSON));
  }

  @Test
  public void shouldReturnUpdatedTicket() throws Exception {
    when(ticketService.update(TICKET)).thenReturn(TICKET);

    mvc.perform(put("/tickets")
      .content(TICKET_JSON)
      .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().json(TICKET_JSON));
  }
}